import React, { useState, useEffect } from 'react';
import dayjs from 'dayjs';
import LottieControl from '../LottieControl';
import styles from './index.module.scss';
import * as animationData from '../../lottie/347-like'

const startTime = dayjs('2018-12-10');

const format = (value) => {
    if (value === undefined || value === '' || value === null) {
        return '--';
    }
    if (value.toString().length === 1) {
        return '0' + value;
    }
    return value;
}

const TimeCount = () => {
    const [nowTime, setNowTime] = useState(dayjs());

    const day = nowTime.diff(startTime, 'day');

    const dayMulti24 = day * 24;
    const hour = nowTime.diff(startTime, 'hour') - dayMulti24;

    const hourMulti24 = (dayMulti24 + hour) * 60;
    const minute = nowTime.diff(startTime, 'minute') -  hourMulti24;

    const minuteMulti24 = (hourMulti24 + minute) * 60;
    const second = nowTime.diff(startTime, 'second') - minuteMulti24;

    useEffect(() => {
        setInterval(() => {
            setNowTime(dayjs());
        }, 1000);
    }, []);

    return (
        <div className={styles.timeCount}>
            <div className={styles.title}>
                我们在一起
            </div>
            <div className={styles.countTime}>
                <span className={styles.item}>{format(day)}天</span>
                <span className={styles.item}>{format(hour)}时</span>
                <span className={styles.item}>{format(minute)}分</span>
                <span className={styles.item}>{format(second)}秒</span>
            </div>
            <div className={styles.info}>
                <div className={styles.names}>
                    <span className={styles.name}>博勋</span>
                    <LottieControl animationData={animationData} size={30} />
                    <span className={styles.name}>晓丽</span>
                </div>
                <div className={styles.start}>
                    <span className={styles.since}>since</span>
                    <span className={styles.time}>{startTime.format('YYYY.MM.DD')}</span>
                </div>
            </div>
        </div>
    );
};

export default TimeCount;
