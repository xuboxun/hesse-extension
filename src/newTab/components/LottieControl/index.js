import React from "react";
import Lottie from 'react-lottie';

const LottieControl = (props) => {
    const {
        animationData,
        size
    } = props;

    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: animationData.default,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice'
        }
    };

    return (
        <Lottie
            options={defaultOptions}
            height={size}
            width={size}
            isStopped={false}
            isPaused={false}
        />
    )
}

export default LottieControl;