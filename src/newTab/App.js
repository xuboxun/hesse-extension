import React from 'react';
import TimeCount from "./components/TimeCount";

function App() {
    return (
        <div>
            <TimeCount />
        </div>
    )
}

export default App;