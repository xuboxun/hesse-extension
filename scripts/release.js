const fs = require('fs');
const path = require('path');
const process = require('child_process');
const JSZip = require('jszip');
const manifest = require('../public/manifest');

const zip = new JSZip();

const releaseName = `love-hesse-release-${manifest.version}.zip`;

const distPath = path.resolve(__dirname, '../dist');

const releasePath = path.join(__dirname, '../release', releaseName);

console.log('source dir: ', distPath);
console.log('target dir: ', releasePath);

const shellStr = `cd ${distPath} && zip -q -r ${releasePath} *`;

process.exec(shellStr,function (error, stdout, stderr) {
    if (error) {
        console.log('exec error: ' + error);
    } else {
        console.log(`${releaseName} generate success`);
    }
});

// node执行的压缩貌似有问题

// readDir(zip, distPath);
//
// function readDir(obj, dir) {
//     let files = fs.readdirSync(dir);
//     files.forEach(function (fileName) {
//         let fillPath = dir + '/' + fileName;
//         let file = fs.statSync(fillPath);
//         if (file.isDirectory()) {
//             let dirlist = zip.folder(fileName);
//             readDir(dirlist, fillPath);
//         } else {
//             obj.file(fileName, fs.readFileSync(fillPath));
//         }
//      });
// }
//
// zip
//     .generateNodeStream({
//         type:'nodebuffer',
//         streamFiles:true
//     })
//     .pipe(fs.createWriteStream(releasePath))
//     .on('finish', function () {
//         console.log(`${releaseName} generate success`);
//     });
